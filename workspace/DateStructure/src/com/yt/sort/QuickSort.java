package com.yt.sort;

import java.util.Arrays;

public class QuickSort {

	public static void main(String[] args) {
		int[] arr = {-9,78,0,23,-567,70,-1,900,4561};
		
		
		quickSort(arr, 0, arr.length - 1);
		System.out.println("arr=" + Arrays.toString(arr));

	}
	
	public static void quickSort(int[] arr, int left, int right){
		int l = left;//左下标
		int r = right;//右下标
		int pivot = arr[(left + right) / 2];//pivot得到数组中间的值
		int temp = 0;
		
		//while循环的目的：让比picot值小放到左边
		//比pivot值大放到右边
		while(l < r){
			// 在pivot左边一直找，找到一个大于或等于pivot值，才退出
			while(arr[l] < pivot){
				l += 1;
			}
			// 在pivot右边一直找，找到一个小于或等于pivot值，才退出
			while(arr[r] > pivot){
				r -= 1;
			}
			// 如果l>=r说明pivot的左右两边的值，已经按照左边全部是小于或等于pivot值
			// 右边全部是大于等于pivot值
			if(l >= r){
				break;
			}
			// 交换
			temp = arr[l];
			arr[l] = arr[r];
			arr[r] = temp;
			
			// 注意：先交换，再进行下面的移位置
			
			// 如果交换完后，发现这个arr[l]==pivot值，则--，因为相等的时候没有必要比较，直接移动另外一边的下标
			// 同理另外一中情况也是
			if(arr[l]== pivot){
				r -= 1;
			}
			if (arr[r] == pivot) {
				l += 1;
			}
			
			

		}
		
		// 如果 l==r,必须l++,r--,否则会出现栈溢出
		// (不太能理解）
		if (l == r) {
			l++;
			r--;
		}
		//向左递归
		if(left < r){
			quickSort(arr, left, r);
		}
		//向右递归
		if (right > l) {
			quickSort(arr, l, right);
		}
		
	}

}
