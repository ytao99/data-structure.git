package com.yt.kruskal;

import java.util.Arrays;

public class KruskalCase {

	private int edgeNum;// 边的个数
	private char[] vertexs;// 顶点数组，记录顶点数据
	private int[][] matrix;// 邻接矩阵，记录图的信息
	// 使用INF表示两个顶点不能联通
	private static final int INF = Integer.MAX_VALUE;

	public static void main(String[] args) {
		char[] vertexs = { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };
		// 邻接矩阵
		int matrix[][] = {
				/* A */ /* B */ /* C *//* D *//* E *//* F *//* G */
				/* A */ { 0, 12, INF, INF, INF, 16, 14 }, /* B */ { 12, 0, 10, INF, INF, 7, INF },
				/* C */ { INF, 10, 0, 3, 5, 6, INF }, /* D */ { INF, INF, 3, 0, 4, INF, INF },
				/* E */ { INF, INF, 5, 4, 0, 2, 8 }, /* F */ { 16, 7, 6, INF, 2, 0, 9 },
				/* G */ { 14, INF, INF, INF, 8, 9, 0 } };

		// 创建实例
		KruskalCase kruskalCase = new KruskalCase(vertexs, matrix);
		// 输出
		kruskalCase.print();
		
		/*
		EData[] edges = kruskalCase.getEdges();
//		[EData [start=A, end=B, weight=12], EData [start=A, end=F, weight=16], EData [start=A, end=G, weight=14],。。。。。
		System.out.println("排序前=" + Arrays.toString(edges));//未排序
		kruskalCase.sortEdges(edges);
		//排序后=[EData [start=E, end=F, weight=2], EData [start=C, end=D, weight=3], EData [start=D, end=E, weight=4]...
		System.out.println("排序后=" + Arrays.toString(edges));
		*/
		
		kruskalCase.kruskal();

	}

	// 构造器
	public KruskalCase(char[] vertxs, int[][] matrix) {
		// 初始化顶点数和边的个数
		int vlen = vertxs.length;

		// 初始化顶点，复制拷贝的方式
		this.vertexs = new char[vlen];
		for (int i = 0; i < vlen; i++) {
			this.vertexs[i] = vertxs[i];
		}

		// 初始化边，复制拷贝的方式
		this.matrix = new int[vlen][vlen];
		for (int i = 0; i < vlen; i++) {
			for (int j = 0; j < vlen; j++) {
				this.matrix[i][j] = matrix[i][j];
			}
		}

		// 统计边的条数
		for (int i = 0; i < vlen; i++) {
			for (int j = i+1; j < vlen; j++) {//注意j的取值，除去统计自己
				if (this.matrix[i][j] != INF) {
					edgeNum++;
				}
			}
		}

	}

	// 打印邻接矩阵
	public void print() {
		System.out.println("邻接矩阵为：\n");
		for (int i = 0; i < vertexs.length; i++) {
			for (int j = 0; j < vertexs.length; j++) {
				System.out.printf("%12d", matrix[i][j]);
			}
			System.out.println();
		}
	}

	/**
	 * 功能：对边进行排序处理，冒泡排序
	 * 
	 * @param edges
	 *            边的集合
	 */
	private void sortEdges(EData[] edges) {
		EData temp;
		boolean flag = false;// 默认表示不需要再比较
		for (int i = 0; i < edges.length - 1; i++) {
			for (int j = 0; j < edges.length - 1 - i; j++) {
				if (edges[j].weight > edges[j + 1].weight) {
					flag = true;// 有比较，修改标志位
					temp = edges[j];
					edges[j] = edges[j+1];
					edges[j+1] = temp;
				}
				
			}
			if (!flag) {
				break;//没有比较之后，退出循环
			} else {
				flag = true;
			}
		}
	}
	
	/**
	 * 
	 * @param ch 顶点的值，比如“A”，“B”。。。
	 * @return 返回ch顶点对应的下标，如果找不到，返回-1
	 */
	private int getPosition(char ch){
		for(int i=0; i<vertexs.length; i++){
			if (vertexs[i] == ch) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * 功能：获取图中边，放到EDate[]数组中，后面我们需要遍历该数组
	 * 是通过matrix邻接矩阵来获取
	 * 
	 * @return EData[] 形式：[['A','B',12],['B','F',7],......]
	 */
	private EData[] getEdges(){
		int index = 0;
		EData[] edges = new EData[edgeNum];
		for(int i=0; i<vertexs.length; i++){
			for(int j=i+1; j<vertexs.length; j++){
				if (matrix[i][j] != INF) {
					edges[index++] = new EData(vertexs[i], vertexs[j], matrix[i][j]);
				}
			}
		}
		return edges;
	}
	
	//难点
	/**
	 * 功能：获取下标为i的顶点的终点，用于后面判断两个顶点的终点是否相同
	 * @param ends 该数组记录了各个顶点对应的终点是哪个，ends数组是在遍历的过程中逐渐形成的
	 * @param i 表示传入的顶点对应的下标
	 * @return 返回下标为i的这个顶点对应的终点的下标
	 */
	private int getEnd(int[] ends, int i){
		while(ends[i] != 0){
			i=ends[i];
		}
		return i;
	}

	public void kruskal(){
		int index=0;//表示最后结果数组的索引
		int[] ends = new int[edgeNum];//用于保存“已有最小生成树”中的每个顶点在最小生成树中的终点
		//创建结果数组，保存最后的最小生成树
		EData[] rets = new EData[edgeNum];
		
		//获取图中所有边的集合
		EData[] edges = getEdges();
		System.out.println("图的边的集合=" + Arrays.toString(edges) + " 共" + edges.length);
		
		//按照边的权值大小进行排序，从小到大
		sortEdges(edges);
		
		//遍历edges数组，将边添加到最小生成树中时，判断准备加入的边是否形成了回路
		//如果没有，就加入rets,否则不能加入
		for(int i=0; i<edgeNum; i++){
			//获取到第i条边的第一个顶点（起点）
			int p1 = getPosition(edges[i].start);
			//获取到第i条边的第2个顶点
			int p2 = getPosition(edges[i].end);
			
			//获取p1这个顶点在已有最小生成树中的终点
			int m = getEnd(ends, p1);
			//获取p2这个顶点在已有最小生成树中的终点
			int n = getEnd(ends, p2);
			//是否构成回路
			if (m != n) {
				//没有构成回路
				ends[m] = n;//设置m在“已有最小生成树”中的终点
				rets[index++] = edges[i];//有一条边加入到rets数组
			}
		}
		
		//统计并打印“最小生成树”，输出rets
		System.out.println("最小生成树为：");
		for(int i=0; i<index; i++){
			System.out.println(rets[i]);
		}
		
	}
	
}

// 创建一个类，EData，它的对象实例就表示一条边
class EData {
	char start;// 边的一个点
	char end;// 边的另外一个点
	int weight;// 边的权重

	// 构造器
	public EData(char start, char end, int weight) {
		this.start = start;
		this.end = end;
		this.weight = weight;
	}
	// 重写toString方法

	@Override
	public String toString() {
		return "EData [<" + start + ", " + end + ">= " + weight + "]";
	}

}
