package com.yt.tree;

public class ArrBinaryTreeDemo {

	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5,6,7};
		//创建一个ArrBinaryTree
		ArrBinaryTree arrBinaryTree = new ArrBinaryTree(arr);
//		arrBinaryTree.preOrder(0);//注意这里要传入参数0，使用重载的方法来避免重这里传入参数
		//arrBinaryTree.preOrder();
		arrBinaryTree.infixOrder();
		arrBinaryTree.postOrder();

	}

}

//编写ArrBinaryTree类，实现顺序存储二叉树
class ArrBinaryTree{
	// 存储数据结点的数组
	private int[] arr;

	public ArrBinaryTree(int[] arr) {
		//super();
		this.arr = arr;
	}
	
	// 重载preOrder()方法
	public void preOrder(){
		this.preOrder(0);//这里将参数为0的根结点传入，不用在函数调用的时候传入参数
	}
	
	// 重载infixOrder()方法
	public void infixOrder() {
		this.infixOrder(0);// 这里将参数为0的根结点传入，不用在函数调用的时候传入参数
	}
	
	// 重载postOrder()方法
	public void postOrder() {
		this.postOrder(0);// 这里将参数为0的根结点传入，不用在函数调用的时候传入参数
	}
	
	// 编写方法实现顺序存储二叉树的前序遍历
	/**
	 * 
	 * @param index 表示数组的小标，从0开始
	 */
	
	public void preOrder(int index){
		//如果数组为空，和数组长度为0，不能遍历
		if (arr == null && arr.length == 0) {
			System.out.println("数组为空，不能进行前序遍历");
		}
		
		// 输出当前这个元素
		System.out.println(arr[index]);
		
		//向左子树进行递归
		// 注意需要判断是否超过数组的长度
		if (arr.length > (2*index + 1)) {
			preOrder(2*index + 1);
		}
		
		//向右子树进行递归
		if (arr.length > (2*index + 2)) {
			preOrder(2*index + 2);
		}
		
	}
	
	// 编写方法实现顺序存储二叉树的中序遍历
	public void infixOrder(int index){
		//判断数组是否为空
		if (arr == null && arr.length == 0) {
			System.out.println("数组为空，不能进行遍历");
		}
		
		//向左递归子树
		if (arr.length > (2*index + 1)) {
			infixOrder(2*index +1);
		}
		//输出当前结点
		System.out.println(arr[index]);
		//向右递归子树
		if (arr.length > (2*index + 2)) {
			infixOrder(index*2 + 2);
		}
	}
	
	// 编写方法实现顺序存储二叉树的后序遍历
	public void postOrder(int index){
		//判断数组是否为空
		if (arr == null && arr.length == 0) {
			System.out.println("数组为空，不能进行遍历");
		}
		
		//向左递归子树
		if (arr.length > (2*index + 1)) {
			postOrder(2*index +1);
		}
		
		//向右递归子树
		if (arr.length > (2 * index + 2)) {
			postOrder(index * 2 + 2);
		}
		// 输出当前结点
		System.out.println(arr[index]);
	}
}
