package com.yt.kmp;

import java.util.Arrays;

public interface KMPAlgorithm {
	public static void main(String[] args) {
		// 测试暴力匹配算法
		String str1 = "BBC ABCDAB ABCDABCDABDE";
		String str2 = "ABCDABD";
//		String str2 = "BBC";
		
		int[] next = kmpNext("ABCDABD");
		System.out.println("next=" + Arrays.toString(next));
		
		int index = kmpSearch(str1, str2, next);
		System.out.println("index=" + index);

	}
	
	//写出kmp搜索算法
	/**
	 * 
	 * @param str1 原字符串
	 * @param str2 子串
	 * @param next 部分匹配表，子串对应的部分匹配表
	 * @return 如果是-1表示没有找到匹配值，否则返回第一个匹配的位置
	 */
	public static int kmpSearch(String str1, String str2, int[] next){
		
		// 遍历
		for(int i=0, j=0; i<str1.length();i++){
			
			//需要考虑str1.charAt(i)！=str2.charAt(j)
			//kmp算法的核心
			while(j>0 && str1.charAt(i)!=str2.charAt(j)){
				j = next[j-1];//难点
			}
			
			if (str1.charAt(i)==str2.charAt(j)) {
				j++;
			}
			
			if (j==str2.length()) {
				return i-j+1;
			}
		}
		return -1;
	}
	
	//获取一个字符串（子串）部分匹配值表
	public static int[] kmpNext(String dest){
		//创建一个next数组保存部分匹配值
		int[] next = new int[dest.length()];
		next[0] = 0;//如果字符串的长度为1，则部分匹配值就是0
		for(int i=1, j=0; i<dest.length(); i++){//注意i是从1开始的
			//当dest.charAt(i)!=dest.charAt(j),需要从next[j-1]获取新的j
			//直到我们发现dest.charAt(i)==dest.charAt(j)成立才退出
			//这是kmp算法的核心
			while(j>0 && dest.charAt(i)!=dest.charAt(j)){
				j = next[j-1];
			}
			//dest.charAt(i)==dest.charAt(j)成立时，满足部分匹配值j就+1
			if (dest.charAt(i)==dest.charAt(j)) {
				j++;
			}
			next[i] = j;//不好理解
		}
		return next;
	}
	

}
