package com.yt.search;

import java.util.ArrayList;
import java.util.List;

public class BinarySearch1 {

	public static void main(String[] args) {
		int arr[] = { 1, 8, 10, 10,10,10,89, 1000, 1234 };

		List<Integer> resIndexList = binarySearch1(arr, 0, arr.length -1, 10);
		System.out.println("resIndexList=" + resIndexList);

	}
	
	/*
	 * 思路分析
	 * 1.找到mid值时，不要马上返回
	 * 2.向mid索引值的左边扫描，将所有相同的值的下标加入到集合ArrayList中
	 * 3.向mid索引值的右边扫描，将所有相同的值的下标加入到集合ArrayList中
	 * 4.将ArrayList返回
	 */
	
	public static ArrayList<Integer> binarySearch1(int[] arr, int left, int right, int findValue) {
		// 当left>right时，说明递归整个数组、但是没有找到
		if (left > right) {
			return new ArrayList<Integer>();
		}

		int mid = (left + right) / 2;
		int midValue = arr[mid];
		if (findValue > midValue) {
			// 向右递归
			return binarySearch1(arr, mid + 1, right, findValue);
		} else if (findValue < midValue) {
			// 向左递归
			return binarySearch1(arr, left, mid - 1, findValue);
		} else {
			/*
			 * 思路分析 1.找到mid值时，不要马上返回 2.向mid索引值的左边扫描，将所有相同的值的下标加入到集合ArrayList中
			 * 3.向mid索引值的右边扫描，将所有相同的值的下标加入到集合ArrayList中 4.将ArrayList返回
			 */

			ArrayList<Integer> resIndexList = new ArrayList<Integer>();
			// 向左边扫描
			int temp = mid - 1;
			while (true) {
				if (temp < 0 || arr[temp] != findValue) {
					break;// 退出循环
				}
				// 否则，将temp 放入到resIndexList中
				resIndexList.add(temp);
				temp--;// temp左移
			}
			resIndexList.add(mid);// 将中间的放入

			// 向mid索引值的右边扫描，将所有相同的值的下标加入到集合ArrayList中
			temp = mid + 1;
			while (true) {
				if (temp > (arr.length - 1) || arr[temp] != findValue) {
					break;// 退出循环
				}
				// 否则，将temp 放入到resIndexList中
				resIndexList.add(temp);
				temp++;// temp右移
			}
			return resIndexList;
		}
	}

}
