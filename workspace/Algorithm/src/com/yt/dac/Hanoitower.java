package com.yt.dac;

public class Hanoitower {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		hanoiTower(2,'A', 'B', 'C');

	}
	
	public static void hanoiTower(int num, char a, char b, char c){
		//如果只有一个盘
		if (num == 1) {
			System.out.println("第" + num + "个盘从 " + a + "->" + c);
		} else {
			//如果n>=2，总是可以看做两个盘组成：最下边的一个盘；除去最下边的所有盘
			//1，先把最上边的所有盘A->B,移动过程会使用到C
			hanoiTower(num-1, a, c, b);
			//2.把最下边的盘A-C
			System.out.println("第" + num + "个盘，从" + a + "->" + c);
			//3.把B塔中的所有盘B->C,移动过程中会使用的a
			hanoiTower(num-1, b, a, c);
		}
	}

}
