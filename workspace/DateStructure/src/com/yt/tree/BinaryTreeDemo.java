package com.yt.tree;

public class BinaryTreeDemo {

	public static void main(String[] args) {
		// 第三步：将结点连接形成二叉树（在main方法中实现）
		BinaryTree binaryTree = new BinaryTree();
		//创建需要的结点
		HeroNode root = new HeroNode(1, "宋江");
		HeroNode heroNode2 = new HeroNode(2, "吴用");
		HeroNode heroNode3 = new HeroNode(3, "卢俊义");
		HeroNode heroNode4 = new HeroNode(4, "关胜");
		HeroNode heroNode5 = new HeroNode(5, "林冲");
		
		// 注意
		binaryTree.setRoot(root);
		
		//手动创建二叉树
		root.setLeft(heroNode2);
		root.setRight(heroNode3);
		heroNode3.setLeft(heroNode4);
		heroNode3.setRight(heroNode5);
/*		
		//测试遍历
		System.out.println("前序遍历");
		binaryTree.preOrder();// 1 2 3 4 5
		
		System.out.println("中序遍历");
		binaryTree.infixOrder();// 2 1 4 3 5
		
		System.out.println("后序遍历");
		binaryTree.postOrder();// 2 4 5 3 1
		*/
		System.out.println("zhong序查找方式");
		
		HeroNode resNode = binaryTree.infixOrderFind(5);
		if (resNode != null) {
			System.out.printf("找到结点，信息为no=%d,name=%s",resNode.getNo(),resNode.getName());
		}else {
			System.out.println("没有找到需要的结点");
		}
		
		//测试删除结点操作
		System.out.println();
		System.out.println("删除前，前序遍历");
		binaryTree.preOrder();
		binaryTree.delNode(4);
		System.out.println("删除后，前序遍历");
		binaryTree.preOrder();

	}
	

}
// 第一步：先创建HeroNode结点
// 第二步：创建二叉树
// 第三步：将结点连接形成二叉树（在main方法中实现）

// 第二步：创建二叉树
class BinaryTree{
	// 定义根结点
	private HeroNode root;
	
	// 创建根结点
	public void setRoot(HeroNode root){
		this.root = root;
	}
	
	// 前序遍历
	public void preOrder(){
		if (this.root != null) {
			this.root.preOrder();
		} else {
			System.out.println("二叉树为空，不能遍历");
		}
	}
	
	// 中序遍历
	public void infixOrder(){
		if (this.root != null) {
			this.root.infixOrder();
		} else {
			System.out.println("二叉树为空，不能遍历");
		}
	}
	
	// 后序遍历
	public void postOrder(){
		if (this.root != null) {
			this.root.postOrder();
		} else {
			System.out.println("二叉树为空，不能遍历");
		}
	}
	
	//前序遍历查找
	public HeroNode preOrderFind(int no){
		if (root != null) {
			return root.preOrderFind(no);
		}else {
			return null;
		}
	}
	
	//中序遍历查找
	public HeroNode infixOrderFind(int no){
		if (root != null) {
			return root.infixOrderFind(no);
		} else {
			return null;
		}
	}
	
	// 后序遍历查找
	public HeroNode postOrderFind(int no){
		if (root != null) {
			return root.postOrderFind(no);
		} else {
			return null;
		}
	}
	
	// 删除结点
	public void delNode(int no){
		if (root == null) {
			System.out.println("空树，不能删除");
		} else {
			//不是空树，可以进行删除操作
			//如果只有一个root结点，判断是不是要删除的结点
			if (root.getNo() == no) {
				root = null;
			} else {
				// 不止一个结点
				// 递归删除
				root.delNode(no);
			}
		}
	}
	
}



//第一步：先创建HeroNode结点
class HeroNode{
	private int no;
	private String name;
	private HeroNode left;//左子结点，默认值为null
	private HeroNode right;//右子结点
	
	public HeroNode(int no, String name) {
		
		this.no = no;
		this.name = name;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HeroNode getLeft() {
		return left;
	}

	public void setLeft(HeroNode left) {
		this.left = left;
	}

	public HeroNode getRight() {
		return right;
	}

	public void setRight(HeroNode right) {
		this.right = right;
	}

	@Override
	public String toString() {
		return "HeroNode [no=" + no + ", name=" + name + "]";
	}
	
	// 编写前序遍历的方法
	public void preOrder(){
		// 输出当前结点
		System.out.println(this);
		
		//递归遍历左子结点
		if (this.left != null) {
			this.left.preOrder();
		}
		
		//递归遍历右子结点
		if (this.right != null) {
			this.right.preOrder();
		}
	}
	
	//编写中序遍历的方法
	public void infixOrder(){
		//递归遍历左子结点
		if (this.left != null) {
			this.left.infixOrder();
		}
		
		//输出当前结点
		System.out.println(this);
		
		//递归遍历右子结点
		if (this.right != null) {
			this.right.infixOrder();
		}
	}
	
	//后序遍历
	public void postOrder(){
		//遍历左子结点
		if (this.left != null) {
			this.left.postOrder();
		}
		
		// 遍历右子结点
		if (this.right != null) {
			this.right.postOrder();
		}
		
		// 输出当前结点
		System.out.println(this);
	}
	
	//前序查找
	/**
	 * 
	 * @param no 查找的no
	 * @return 如果找到就返回该结点，没有找到就返回null
	 */
	public HeroNode preOrderFind(int no){
		System.out.println("进入前序遍历");
		//判断当前结点是不是要查找的，如果是则返回
		if (this.no == no) {
			return this;
		}
		//1.判断左子结点是否为空，如果不为空，则递归前序查找
		//2.递归前序查找，找到之后返回
		HeroNode resHeroNode = null;//用于保存查找的结果结点
		if (this.left != null) {
			resHeroNode = this.left.preOrderFind(no);
		}
		if (resHeroNode != null) {
			return resHeroNode;//说明在左子树中找到			
		}
		
		//1.左递归之后没有找到，继续判断右子树
		//2.当前结点的右子结点不为空，则继续右递归前序查找
		if (this.right != null) {
			resHeroNode = this.right.preOrderFind(no);
		}
		
		return resHeroNode;//说明在右子树中找到，右子树也没有找到的话，返回初始值null
	}
	
	//中序查找
	public HeroNode infixOrderFind(int no){
		//判断当前结点的左子结点是否为空，如果不为空，则递归中序查找
		HeroNode resNode = null;
		if (this.left != null) {
			resNode = this.left.infixOrderFind(no);
		}
		if (resNode != null) {
			return resNode;//说明找到
		}
		
		System.out.println("进入中序遍历");
		//如果找到，则返回当前结点
		if (this.no == no) {
			return this;
		}
		
		// 否则继续进行右递归中序查找
		if (this.right != null) {
			resNode = this.right.infixOrderFind(no);
		}
		return resNode;
	}
	
	//后序遍历查找
	public HeroNode postOrderFind(int no){
		// 如果当前结点的左子结点不为空，则递归后序查找
		HeroNode resNode = null;
		if (this.left != null) {
			resNode = this.left.postOrderFind(no);
		}
		if (resNode != null) {
			return resNode;
		}
		// 左子树没有找到，则判断右子树是否找到，没有找到就继续递归后序查找
		if (this.right != null) {
			resNode = this.right.postOrderFind(no);
		}
		if (resNode != null) {
			return resNode;
		}
		
		System.out.println("进入后序遍历查找");
		// 如果左右子树都没有找到，就比较当前结点是不是
		if (this.no == no) {
			return this;
		}
		
		return resNode;
	}
	
	// 递归删除结点
	public void delNode(int no){
		// 如果当前结点的左子结点不为空，并且左子结点就是要删除的结点，就将this.left=null;并且返回结束递归；
		if (this.left != null && this.left.no==no) {
			this.left = null;
			return;
		}
		// 如果当前结点的右子结点不为空，并且右子结点就是要删除的结点，就是将this.right=null;并且返回，结束递归；
		if (this.right != null && this.right.no == no) {
			this.right = null;
			return;
		}
		// 前面的步骤都没有删除，继续向左递归删除
		if (this.left != null) {
			this.left.delNode(no);
		}
		// 前面的步骤都没有删除，继续向右递归删除
		if (this.right != null) {
			this.right.delNode(no);
		}
		
	}
	
	
}
