package com.yt.sort;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class SelectSort {

	public static void main(String[] args) {
		
		/*
		int[] arr = { 101, 34, 119, 1 ,123,654};
		System.out.println("排序前");
		System.out.println(Arrays.toString(arr));
		selectSort(arr);
		System.out.println("排序后");
		System.out.println(Arrays.toString(arr));
		*/
		
		// 测试选择排序的速度
		// 创建一个80000个随机的数组
		int[] arr = new int[80000];
		for (int i = 0; i < 80000; i++) {
			arr[i] = (int) (Math.random() * 800000);
		}

		// System.out.println(Arrays.toString(arr));
		Date date1 = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date1Str = simpleDateFormat.format(date1);
		System.out.println("排序前的时间是=" + date1Str);

		// 测试选择排序
		selectSort(arr);
		// System.out.println(Arrays.toString(arr));

		Date date2 = new Date();
		// SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd
		// HH:mm:ss");
		String date2Str = simpleDateFormat.format(date2);
		System.out.println("排序后的时间是=" + date2Str);
		// 大概是1秒的时间
		

	}

	// 选择排序
	public static void selectSort(int[] arr) {

		// 在推导的过程中我们发现可以使用一个循环
		for (int i = 0; i < arr.length - 1; i++) {
			int minIndex = i;// 假设最小值的索引是0
			int min = arr[i];// 假设第一个数就是最小值，可能是，可能不是
			for (int j = i + 1; j < arr.length; j++) {
				if (min > arr[j]) {// 说明假设错误。假设的值并不是最小值
					min = arr[j];// 重置最小值
					minIndex = j;// 重置最小值的下标minIndex
				}
			}
			// 第一轮循环结束，交换，将最小值放到最前面
			if (minIndex != i) {
				arr[minIndex] = arr[i];// 将一开始假设的最小值（不一定是真正的最小值）换到真正的最小值的位置
				arr[i] = min;// 将真正的最小值放到第一个位置
			}

			//System.out.println("第" + (i+1) +"1轮排序之后");
//			System.out.println(Arrays.toString(arr));
		}
		
		
		// 使用逐步推导的方式来进行学习
		// 第一轮
		// 原始数组: 101， 34， 119,1
		// 第一轮排序：1,34,119,101
		// 算法的学习：由简单到复杂，拆分问题

		/* 每一步的具体推导过程
		// 第一轮排序
		int minIndex = 0;// 假设最小值的索引是0
		int min = arr[0];// 假设第一个数就是最小值，可能是，可能不是
		for (int j = 0 + 1; j < arr.length; j++) {
			if (min > arr[j]) {// 说明假设错误。假设的值并不是最小值
				min = arr[j];// 重置最小值
				minIndex = j;// 重置最小值的下标minIndex
			}
		}
		// 第一轮循环结束，交换，将最小值放到最前面
		if (minIndex != 0) {
			arr[minIndex] = arr[0];// 将一开始假设的最小值（不一定是真正的最小值）换到真正的最小值的位置
			arr[0] = min;// 将真正的最小值放到第一个位置
		}

		System.out.println("第1轮排序之后");
		System.out.println(Arrays.toString(arr));

		// 第2轮排序
		minIndex = 1;// 假设最小值的索引是1
		min = arr[1];// 假设第一个数就是最小值，可能是，可能不是
		for (int j = 1 + 1; j < arr.length; j++) {
			if (min > arr[j]) {// 说明假设错误。假设的值并不是最小值
				min = arr[j];// 重置最小值
				minIndex = j;// 重置最小值的下标minIndex
			}
		}
		// 第2轮循环结束，交换，将最小值放到最前面
		// 补充：如果假设的刚好就是最小值，那么下面两句代码的交换没有意义,用if 语句优化
		if (minIndex != 1) {
			arr[minIndex] = arr[1];// 将一开始假设的最小值（不一定是真正的最小值）换到真正的最小值的位置
			arr[1] = min;// 将真正的最小值放到第一个位置
		}

		System.out.println("第2轮排序之后");
		System.out.println(Arrays.toString(arr));

		// 第3轮排序
		minIndex = 2;// 假设最小值的索引是1
		min = arr[2];// 假设第一个数就是最小值，可能是，可能不是
		for (int j = 2 + 1; j < arr.length; j++) {
			if (min > arr[j]) {// 说明假设错误。假设的值并不是最小值
				min = arr[j];// 重置最小值
				minIndex = j;// 重置最小值的下标minIndex
			}
		}
		// 第3轮循环结束，交换，将最小值放到最前面
		// 补充：如果假设的刚好就是最小值，那么下面两句代码的交换没有意义,用if 语句优化
		if (minIndex != 2) {
			arr[minIndex] = arr[2];// 将一开始假设的最小值（不一定是真正的最小值）换到真正的最小值的位置
			arr[2] = min;// 将真正的最小值放到第一个位置
		}

		System.out.println("第3轮排序之后");
		System.out.println(Arrays.toString(arr));
		*/

	}

}
