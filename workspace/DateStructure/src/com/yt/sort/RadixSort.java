package com.yt.sort;

import java.util.Arrays;

public class RadixSort {

	public static void main(String[] args) {
		int arr[] = {53,3,542,748,14,214};
		radixSort(arr);

	}
	
	// 基数排序
	public static void radixSort(int[] arr) {
		
		// 根据下面推导，得到最终代码
		
		// 1.得到数组中最大的数的位数
		int max = arr[0];
		for(int i= 1 ; i < arr.length; i++){
			if (max < arr[i]) {
				max =arr[i];
			}
		}
		// 2.得到最大数是几位数
		// 小技巧
		int maxLength = (max + "").length();//决定有几轮排序
		
		//System.out.println(maxLength);		
				
		// 定义一个二维数组，表示10个桶，每个桶表示一个一维数组
		// 说明
		// 1.二维数组包含10个一维数组
		// 2.为了防止在放入数的时候，数据溢出，则每个一维数组，大小定义为arr.length
		// 3.说明基数排序是使用空间换取时间的经典排序算法
		int[][] bucket = new int[10][arr.length];
		
		// 为了记录每个桶中实际存放了多少个数据，我们定义一个一维数组来记录各个桶的每次放入的数据个数
		// 比如：bucketElementCounts[0]记录的就是bucket[0] 桶中放入数据个数
		int[] bucketElementCounts = new int[10];

		for (int i = 0, n = 1; i < maxLength; i++, n = n * 10) {
			// 针对每个元素的对应位进行排序处理，第一次个位，第二次十位，第三次百位
			for (int j = 0; j < arr.length; j++) {
				// 取出每个元素的对应位的数值,也等价于表示第几个桶的位置
				int digitOfElement = arr[j] / n % 10;
				// 放入到对应的桶中
				// 难点
				bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[j];
				bucketElementCounts[digitOfElement]++;// 表示又需要再加一个数
			}
			// 按照这个桶的顺序（一维数组的下标依此取出数据，放入原来数值）
			int index = 0;
			// 遍历每一个桶，并将桶中的数据，放入到原数组中
			for (int k = 0; k < bucketElementCounts.length; k++) {
				// 如果桶中有数据，我们才放入到原数组中
				if (bucketElementCounts[k] != 0) {
					// 循环该桶，即第K个桶
					for (int l = 0; l < bucketElementCounts[k]; l++) {
						// 取出元素放到arr
						arr[index++] = bucket[k][l];
					}
				}
				// 第i+1轮处理完成之后，需要将每个bucketElementCounts[k]=0；置零
				// 很重要
				bucketElementCounts[k] = 0;
			}

			System.out.println("第" + (i + 1) +"轮，对个位数值的排序处理arr=" + Arrays.toString(arr));
		}
		
		
		/*
		
		// 第一轮排序：针对每个元素的个位进行排序处理
		for(int j=0; j<arr.length; j++){
			// 取出每个元素的个位的数值,也等价于表示第几个桶的位置
			int digitOfElement = arr[j] % 10;
			// 放入到对应的桶中
			// 难点
			bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[j];
			bucketElementCounts[digitOfElement]++;// 表示又需要再加一个数
		}
		// 按照这个桶的顺序（一维数组的下标依此取出数据，放入原来数值）
		int index = 0;
		// 遍历每一个桶，并将桶中的数据，放入到原数组中
		for(int k = 0; k < bucketElementCounts.length; k++){
			// 如果桶中有数据，我们才放入到原数组中
			if (bucketElementCounts[k] != 0) {
				// 循环该桶，即第K个桶
				for(int l = 0; l < bucketElementCounts[k]; l++){
					// 取出元素放到arr
					arr[index++] = bucket[k][l];
				}
			}
			// 第一轮处理完成之后，需要将每个bucketElementCounts[k]=0；置零
			// 很重要
			bucketElementCounts[k] = 0;
		}
		
		System.out.println("第一轮，对个位数值的排序处理arr=" + Arrays.toString(arr));
		
		// 第2轮排序：针对每个元素的十位进行排序处理
		for (int j = 0; j < arr.length; j++) {
			// 取出每个元素的十位的数值,也等价于表示第几个桶的位置
			int digitOfElement = arr[j] / 10 % 10;
			// 放入到对应的桶中
			// 难点
			bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[j];
			bucketElementCounts[digitOfElement]++;// 表示又需要再加一个数
		}
		// 按照这个桶的顺序（一维数组的下标依此取出数据，放入原来数值）
		index = 0;
		// 遍历每一个桶，并将桶中的数据，放入到原数组中
		for (int k = 0; k < bucketElementCounts.length; k++) {
			// 如果桶中有数据，我们才放入到原数组中
			if (bucketElementCounts[k] != 0) {
				// 循环该桶，即第K个桶
				for (int l = 0; l < bucketElementCounts[k]; l++) {
					// 取出元素放到arr
					arr[index++] = bucket[k][l];
				}
			}
			bucketElementCounts[k] = 0;
		}

		System.out.println("第2轮，对个位数值的排序处理arr=" + Arrays.toString(arr));
		
		// 第3轮排序：针对每个元素的百位进行排序处理
		for (int j = 0; j < arr.length; j++) {
			// 取出每个元素的百位的数值,也等价于表示第几个桶的位置
			int digitOfElement = arr[j] / 100 % 10;
			// 放入到对应的桶中
			// 难点
			bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[j];
			bucketElementCounts[digitOfElement]++;// 表示又需要再加一个数
		}
		// 按照这个桶的顺序（一维数组的下标依此取出数据，放入原来数值）
		index = 0;
		// 遍历每一个桶，并将桶中的数据，放入到原数组中
		for (int k = 0; k < bucketElementCounts.length; k++) {
			// 如果桶中有数据，我们才放入到原数组中
			if (bucketElementCounts[k] != 0) {
				// 循环该桶，即第K个桶
				for (int l = 0; l < bucketElementCounts[k]; l++) {
					// 取出元素放到arr
					arr[index++] = bucket[k][l];
				}
			}
			bucketElementCounts[k] = 0;
		}

		System.out.println("第3轮，对个位数值的排序处理arr=" + Arrays.toString(arr));
		
		*/
		
	}
	

}
