package com.yt.binarysearchnorecursion;

public class BinarySearchNoRecursion {
	
	public static void main(String[] args){
		int[] arr= {1,2,8,10,11,67,100};
		int index = binarySearch(arr, 10);
		System.out.println("index=" + index);
		
		
	}

	/**
	 * 
	 * @param arr 待查找的数组，arr默认是升序的
	 * @param target 需要查找的数
	 * @return 找到target返回数据所在下标，否则返回-1
	 */
	public static int binarySearch(int[] arr, int target){
		int left = 0;
		int right = arr.length - 1;
		
		while(left <= right){
			int mid = (left + right) / 2;
			if (arr[mid]==target) {
				//找到数据
				return mid;
			}else if(arr[mid] > target) {
				right = mid -1;
			} else {
				left = mid + 1;
			}
		}
		return -1;
		
	}
}
