package com.yt.huffmancode;

import java.util.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class HuffmanCode {

	public static void main(String[] args) {
		//测试压缩文件
//		String srcFile = "C://Users/dell/Desktop/test.xml";
//		String dstFlie = "C://Users/dell/Desktop/test.zip";
//		zipFile(srcFile, dstFlie);
//		System.out.println("压缩完成");
//		
		//测试解压文件
		String zipFile = "C://Users/dell/Desktop/test.zip";
		String dstFile = "C://Users/dell/Desktop/test2.xml";
		unZipFile(zipFile, dstFile);
		System.out.println("解压成功");
		
		
		
		/*
		String content = "i like like like java do you like a java";
		byte[] contentBytes = content.getBytes();
		
		byte[] huffmanCodesBytes = huffmanZip(contentBytes);
		System.out.println("压缩之后的结果是" + Arrays.toString(huffmanCodesBytes));
		
		// 测试byteToBitString方法
//		System.out.println(byteToBitString(true,(byte)1));
		
		byte[] decode = decode(huffmanCodes, huffmanCodesBytes);
		//System.out.println(decode);//10101000101111111100100010......
		System.out.println("原来的字符串=" + new String(decode));
		//原来的字符串=iaikal kal kajoe aoeleijkuljoe
		// 注意易错点，不用i++
//		原来的字符串=i like like like java do you like a java 
		*/
		
//		for(byte b : contentBytes){
//			System.out.print(b + " ");
//		}
		
		/*
		System.out.println(contentBytes.length);//40
		
		List<Node> nodes = getNodes(contentBytes);
		System.out.println(nodes);//[Node [data=32, weight=9], Node [data=97, weight=5], Node [data=100, weight=1]
		
		//测试创建的二叉树
		System.out.println("赫夫曼树");
		Node huffmanTreeRoot = createHuffmanTree(nodes);
		System.out.println("前序遍历");
		preOrder(huffmanTreeRoot);
		
		//测试是否生成了对应的赫夫曼编码
//		getCodes(huffmanTreeRoot, "", stringBuilder);//调用太复杂,重载getCodes()方法
		Map<Byte, String> huffmanCodes = getCodes(huffmanTreeRoot);
		System.out.println("生成的赫夫曼编码表=" + huffmanCodes);
		
		byte[] huffmanCodeBytes = zip(contentBytes, huffmanCodes);
		System.out.println("huffmanCodeBytes=" + Arrays.toString(huffmanCodeBytes));
		*/
		
		
	}
	
	// 第十一，编写一个方法，完成对压缩文件的解压
	/**
	 * 
	 * @param zipFile 准备解压的文件
	 * @param dstFile 将文件解压到哪个路径
	 */
	public static void unZipFile(String zipFile, String dstFile){
		// 定义文件输入流
		InputStream is = null;
		//定义一个对象输入流
		ObjectInputStream ois = null;
		//定义文件的输出流
		OutputStream os = null;
		
		try {
			//创建文件输入流
			is = new FileInputStream(zipFile);
			// 创建一个与 is关联的对象输入流
			ois = new ObjectInputStream(is);
			//读取byte数组huffmanBytes
			byte[] huffmanBytes = (byte[])ois.readObject();
			//读取赫夫曼编码表
			Map<Byte, String> huffmanCodes = (Map<Byte, String>) ois.readObject();
			
			//解码
			byte[] bytes = decode(huffmanCodes, huffmanBytes);
			//将bytes数组写入到目标文件
			os = new FileOutputStream(dstFile);
			//写数据到dstFile文件
			os.write(bytes);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} finally {
			try {
				os.close();
				ois.close();
				is.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		}
 	}
	
	//第十，编写方法，将一个文件进行压缩
	/**
	 * 
	 * @param srcFile 传入的压缩文件的全路径
	 * @param dstFile 压缩完成之后存储的文件路径
	 */
	public static void zipFile(String srcFile,String dstFile){
		//创建输出流
		OutputStream os = null;
		ObjectOutputStream oos = null;
		
		// 创建文件的输出流
		FileInputStream is=null;
		
		try {
			// 创建文件的输入流
			is = new FileInputStream(srcFile);
			// 创建一个和源文件大小一样的byte[]
			byte[] b = new byte[is.available()];
			// 读取文件
			is.read(b);
			//直接对源文件压缩
			byte[] huffmanBytes = huffmanZip(b);
			//创建文件的输出流，存放压缩文件
			os = new FileOutputStream(dstFile);
			//创建一个和文件输出流关联的ObjectOutputStream
			oos = new ObjectOutputStream(os);
			//把赫夫曼编码后的字节数组写入压缩文件
			oos.writeObject(huffmanBytes);
			//这里我们以对象流的方式写入赫夫曼编码，是为了以后恢复源文件时使用
			// 注意：一定要把赫夫曼编码写入压缩文件
			oos.writeObject(huffmanCodes);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} finally {
			try {
				is.close();
				os.close();
				oos.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		}
		
	}
	
	
	//第九，编写一个方法，完成对压缩数据的解码
	private static byte[] decode(Map<Byte, String> huffmanCodes, byte[] huffmanBytes){
		//1.先得到huffmanBytes对应的二进制的字符串，形如1010100010111...
		StringBuilder stringBuilder = new StringBuilder();
		//将byte数组转成二进制的字符串
		for(int i = 0; i< huffmanBytes.length; i++){
			byte b = huffmanBytes[i];
			//判断是不是最后一个字节，一个字节等于8位
			boolean flag = (i== huffmanBytes.length -1);//默认是最后一个字节
			stringBuilder.append(byteToBitString(!flag, b));
		}
		//System.out.println("哈夫曼字节数组对应的二进制字符串=" + stringBuilder.toString());//101010001011111111001000101....
		
		// 把字符串按照制定的赫夫曼编码进行解码
		// 把哈夫曼编码进行调换，注意Map中泛型的使用
		Map<String, Byte> map = new HashMap<String,Byte>();
		for(Map.Entry<Byte, String> entry: huffmanCodes.entrySet()){
			map.put(entry.getValue(), entry.getKey());
		}
//		map={000=108, 01=32, 100=97, 101=105, 11010=121, 0011=111, 1111=107, 11001=117, 1110=101, 11000=100, 11011=118, 0010=106}
//		System.out.println("map=" + map);
		
		//创建一个集合，存放byte
		List<Byte> list = new ArrayList<>();
		// i可以理解为一个索引，扫描stringBuilder=1010100010.....
		// 注意：易错  i++不需要写，i一直指向0的位置
		for(int i = 0; i < stringBuilder.length();){
			int count = 1;//计数器
			boolean flag = true;
			Byte b = null;
			while(flag){
				//i不动，让count移动，直到匹配到一个字符
				String key = stringBuilder.substring(i,i+count);
				b = map.get(key);
				if (b == null) {
					//说明没有匹配到
					count++;
				} else {
					// 匹配到
					flag = false;
				}
			}
			list.add(b);
			i += count;//i直接移动到count
		}
		//当for循环结束之后，我们list中存放了所有的字符，”i like like like......“
		//把list中的数据放入到byte[] 并返回
		byte[] b = new byte[list.size()];
		for(int i=0; i<b.length; i++){
			b[i] = list.get(i);
		}	
		return b;
	}
	
	
	//第八，转化成二进制
	/**
	 * 将一个byte转成一个二进制的字符串
	 * @param flag 标志是否需要补全高位，如果是true,表示需要补高位，如果是false表示不需要补全高位，另外如果是最后一个字节，无需补全高位
	 * @param b 传入的byte
	 * @return 是传入byte对应的二进制的字符串（注意是按照补码返回）
	 */
	private static String byteToBitString(Boolean flag,byte b){
		//使用变量保存b
		int temp = b;//将b转成int，转成数值
		// 返回的是temp对应的二进制的补码，还特别长,所以存在截取的问题
		// str=11111111111111111111111111
//		System.out.println("str=" + str);
		// 如果是整数，还存在补全高位
		if (flag) {
			temp |= 256;//按位与256
		}
		String str = Integer.toBinaryString(temp);// 返回的是temp对应的二进制的补码，还特别长,所以存在截取的问题

		if (flag) {
			return str.substring(str.length() - 8);
		} else {			
			return str;
		}
		
		
	}
	
	// 第七，使用一个方法将前面的方法封装起来，便于我们调用
	/**
	 * 
	 * @param bytes 原始的字符串对应的字节数组
	 * @return 进过赫夫曼编码处理后的字节数组（压缩后的数组）
	 */
	private static byte[] huffmanZip(byte[] bytes){
		// 创建字符串对应的list
		List<Node> nodes = getNodes(bytes);
//		System.out.println(nodes);
		//根据nodes创建哈夫曼树
		Node huffmanTreeRoot = createHuffmanTree(nodes);
//		System.out.println(huffmanTreeRoot);
		//对应的哈夫曼编码(根据哈夫曼树得到)
		Map<Byte, String> huffmanCodes = getCodes(huffmanTreeRoot);
		//System.out.println(huffmanCodes);//{32=01, 97=100, 100=11000, 117=11001, 101=1110, 118=11011, 105=101, 121=11010, 106=0010, 107=1111, 108=000, 111=0011}
		// 根据生成的哈夫曼编码，压缩得到压缩后的哈夫曼编码字节数组
		byte[] huffmanCodeBytes = zip(bytes, huffmanCodes);
		return huffmanCodeBytes;
	}
	
	// 第六，编写一个方法，将字符串对应的byte[]数组，通过生成的赫夫曼编码表，返回一个赫夫曼编码压缩后的byte[]
	private static byte[] zip(byte[] bytes,Map<Byte, String> huffmanCodes){
		//1.利用huffmanCodes 将bytes 转成 赫夫曼编码对应的字符串
		StringBuilder stringBuilder = new StringBuilder();
		//2.遍历数组bytes[]
		for(Byte b: bytes){
			stringBuilder.append(huffmanCodes.get(b));
		}
		//System.out.println(stringBuilder.toString());//1101101011010110100101011011101101010010
		
		//将1101101... 转成 byte[]
		
		//统计返回 byte[] huffmanCodeBytes 长度
		//一句话实现：int len = (stringBuilder.length() + 7) / 8
		int len;
		if (stringBuilder.length() % 8 == 0) {
			len = stringBuilder.length() / 8;
		} else {
			len = stringBuilder.length() / 8 + 1;
		}
		
		// 创建 存储压缩后的byte数组
		byte[] huffmanCodeBytes = new byte[len];
		int index = 0;//记录是第几个byte
		for(int i=0; i < stringBuilder.length(); i+=8){//因为是每八位对应一个byte,所以步长+8
			String strByte;
			if (i+8 > stringBuilder.length()) {
				// 不够8位
				strByte = stringBuilder.substring(i);//表示从i开始到最后截取
			} else {
				strByte = stringBuilder.substring(i,i+8);
			}
			//将strByte转成一个byte,放入到huffmanCodeBytes
			huffmanCodeBytes[index] = (byte)Integer.parseInt(strByte,2);//2表示2进制
			index++;
		}
		return huffmanCodeBytes;
		
	}
	
	
	
	//第五.1 ，为了方便调用方便，重载getCodes（）
	private static Map<Byte,String> getCodes(Node root){
		if (root == null) {
			return null;
		}
		//处理左子树
		getCodes(root.left,"0",stringBuilder);
		//处理右子树
		getCodes(root.right,"1",stringBuilder);
		return huffmanCodes;
	}
	
	//第五，生成赫夫曼树对应的赫夫曼编码
	//思路
	//1.将赫夫曼码存放在Map中
	static Map<Byte, String> huffmanCodes= new HashMap<Byte, String>();
	//2.将生成的哈夫曼编码的路径进行拼接，定义一个StringBuilder 存储某个叶子结点的路径
	static StringBuilder stringBuilder = new StringBuilder();
	//第五，生成赫夫曼树对应的赫夫曼编码方法
	/**
	 * 功能：将传入的node结点的所有叶子结点的赫夫曼编码得到，并放入到huffmanCode集合中
	 * @param node 转入的结点
	 * @param code 路径：左子结点表示0 ， 右子结点表示1
	 * @param stringBuilder 用于拼接路径
	 */
	private static void getCodes(Node node, String code, StringBuilder stringBuilder){
		StringBuilder stringBuilder2 = new StringBuilder(stringBuilder);
		//将code（路径）加入到stringBuilder2中
		stringBuilder2.append(code);
		if (node != null) {//如果node==null不处理
			// 判断当前node是叶子结点还是非叶子结点
			if (node.data == null) {
				// 表示非叶子结点
				//递归处理
				//先向左进行递归
				getCodes(node.left, "0", stringBuilder2);
				//再向右进行递归
				getCodes(node.right, "1", stringBuilder2);
			} else {
				//说明是一个叶子结点
				huffmanCodes.put(node.data, stringBuilder2.toString());
			}
		}
	}
	
	
	
	// 第四，编写前序遍历方法
	private static void preOrder(Node root){
		if (root != null) {
			root.preOrder();
		} else {
			System.out.println("空树，不能遍历");
		}
	}
	
	// 第三：可以通过List创建对应的赫夫曼树
		public static Node createHuffmanTree(List<Node> nodes){
			
			while(nodes.size() > 1){
				//排序，从小到大排序
				Collections.sort(nodes);
				//取出第一棵最小的二叉树
				Node leftNode = nodes.get(0);
				//取出第二课最小的二叉树
				Node rightNode = nodes.get(1);
				//创建一棵新的二叉树，它的根结点没有data 值为null,只有权值
				Node parent = new Node(null, leftNode.weight + rightNode.weight);
				parent.left = leftNode;
				parent.right = rightNode;
				
				//注意：将已经处理过的两棵树从nodes中删除
				nodes.remove(leftNode);
				nodes.remove(rightNode);
				//将新的二叉树加入到nodes中
				nodes.add(parent);
			}
			//node最后的结点就是哈夫曼树的根结点
			return nodes.get(0);
			
		}
	
	// 第二：编写一个方法,获得结点信息（字符，权值）
	/**
	 * 
	 * @param bytes 接收字节数组
	 * @return 返回List形式，[Node [data=32, weight=9], Node [data=97, weight=5]]
	 */
	private static List<Node> getNodes(byte[] bytes){
		
		//1.创建一个ArrayList
		ArrayList<Node> nodes = new ArrayList<Node>();
		
		//遍历bytes,统计每一个byte出现的次数，使用java中的map[key,value]
		HashMap<Byte, Integer> counts = new HashMap<>();
		for(byte b : bytes){
			Integer count = counts.get(b);
			
			if (count == null) {
				counts.put(b, 1);
				
			} else {
				counts.put(b, count + 1);
			}
			//System.out.println(counts);
		}
		
		// 把每个键值对转成一个Node对象，并加入到nodes集合
		// 遍历map
		for(Map.Entry<Byte, Integer> entry: counts.entrySet()){
			nodes.add(new Node(entry.getKey(), entry.getValue()));
		}
		return nodes;
	}
	
	

}


//第一： 创建Node，带数据和权值
class Node implements Comparable<Node> {
	Byte data;// 存放数据本身，比如‘a'=>97
	int weight;// 权值，表示字符出现的次数
	Node left;
	Node right;

	public Node(Byte data, int weight) {
		// super();
		this.data = data;
		this.weight = weight;
	}

	@Override
	public int compareTo(Node o) {
		// TODO Auto-generated method stub
		// 从小到大排序
		return this.weight - o.weight;
	}

	@Override
	public String toString() {
		return "Node [data=" + data + ", weight=" + weight + "]";
	}
	
	// 前序遍历
	public void preOrder(){
		System.out.println(this);
		if (this.left != null) {
			this.left.preOrder();
		}
		if (this.right != null) {
			this.right.preOrder();
		}
	}

}
