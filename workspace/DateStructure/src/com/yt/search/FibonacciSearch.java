package com.yt.search;

import java.util.Arrays;

public class FibonacciSearch {
	public static int maxSize = 20;

	public static void main(String[] args) {
		int [] arr = {1,8,10,89,1000,1234};
		System.out.println("index=" + fibSearch(arr, 1));

	}
	
	// 因为后面我们需要使用斐波那契数列，因此需要先获取到斐波那契数列，求出mid = low + F(k-1) -1
	// 用非递归方法得到斐波那契数列
	public static int[] fib() {
		int[] f = new int[maxSize];
		f[0] = 1;
		f[1] = 1;
		for (int i = 2; i < maxSize; i++) {
			f[i] = f[i - 1] + f[i - 2];
		}
		return f;

	}
	
	// 编写斐波那契查找算法
	// 使用非递归的方法
	/**
	 * 
	 * @param arr 传入的数组
	 * @param key 需要查找的关键码（值）
	 * @return 返回对应的下标，没有则返回-1
	 */
	public static int fibSearch(int[] arr, int key){
		int low = 0;
		int high = arr.length -1;
		int k =0;// 表示斐波那契分割数值的下标
		int mid = 0;
		int f[] = fib();// 获取到斐波那契数列
		// high 表示原数组的长度
		// 获取到斐波那契分割下标k
		// 难点
		while(high > f[k] - 1){
			k++;
		}
		// 因为f[k]的值可能大于数组的长度，因此需要使用Arrays类，构造一个新的数组，并指向arr[]
		// 原数组是arr，长度是f[k],如果arr小，f[k]大，那么返回的temp就会用0填充
		int temp[] = Arrays.copyOf(arr, f[k]);
		// 实际上需要使用arr数组最后的数填充temp,不要用0来填充
		for(int i = high + 1; i < temp.length; i++){
			temp[i] = arr[high];
		}
		
		// 使用while循环来处理，找到我们的数key
		while(low <= high){
			mid = low + f[k-1] -1;
			if (key < temp[mid]) {
				// 说明应该继续向数组的前面查找（左边查找）
				high = mid -1;
				// 难点
				// 说明
				// 1.全部元素= 前面的元素 + 后面的元素
				// 2. f[k] = f[k-1] + f[k-2]
				// 因为前面有f[k-1]个元素，所以可以继续继续拆分f[k-1] = f[k-2] + f[k-3]
				// 即在f[k-1] 的前面继续查找 k--
				// 即下次循环mid = f[k-1-1] -1
				k--;
			} else if (key > temp[mid]) {
				//说明向数组的后面查找（右边查找）
				low = mid + 1;
				// 说明
				// 1.全部元素= 前面的元素 + 后面的元素
				// 2. f[k] = f[k-1] + f[k-2]
				// 3.因为后面我们有f[k-2]个元素，所以f[k-1] = f[k-3] + f[k-4]
				// 4.即在f[k-2]的前面进行查找 k = k-2
				// 5.即下次循环mid = f[k-1-2] -1
				k = k -2;
			} else {
				//找到
				// 需要确定返回的是哪个下标
				if (mid <= high) {
					return mid;
				} else {
					return high;
				}
				
			}
		}
		return -1;
		
	}
	
	
	

}
