package com.yt.sort;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class InsertSort {

	public static void main(String[] args) {
//		int[] arr = {101,34,119,1,-1,90};
		// 测试选择排序的速度
		// 创建一个80000个随机的数组
		int[] arr = new int[80000];
		for (int i = 0; i < 80000; i++) {
			arr[i] = (int) (Math.random() * 800000);
		}

		// System.out.println(Arrays.toString(arr));
		Date date1 = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date1Str = simpleDateFormat.format(date1);
		System.out.println("排序前的时间是=" + date1Str);
		
		insertSort(arr);
		
		Date date2 = new Date();
		// SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd
		// HH:mm:ss");
		String date2Str = simpleDateFormat.format(date2);
		System.out.println("排序后的时间是=" + date2Str);
		// 大概是1-2秒的时间

	}
	
	public static void insertSort(int[] arr) {
		//使用for循环简化代码
		for (int i = 1; i < arr.length; i++) {
			// 定义待插入的数,有序列表的后一个数
			int insertVal = arr[i];
			int insertIndex = i - 1;//即arr[1]前面数字的下标
			//给insertVal 找到插入的位置
			//说明：
			//1.insertIndex >= 0 保证在给insertVal 插入位置时不越界
			//2.insertVal < arr[insertIndex] 表示待插入的数，还没有找到插入的位置
			//3.就需要将arr[insertIndex]这个值后移
			while (insertIndex >= 0 && insertVal < arr[insertIndex]) {
				arr[insertIndex + 1] = arr[insertIndex];
				//注意：arr[insertIndex + 1]位置的值已经被保存在insertVal中
				insertIndex--;
			}
			//当推迟while循环时，找到插入位置，insertIndex+1;使用debug加深理解
			//这里判断是否需要赋值
			if(insertIndex + 1 == i) {
				arr[insertIndex + 1] = insertVal;
			}
		
			//System.out.println("第" + i + "轮插入后");
			//System.out.println(Arrays.toString(arr));
		}
		
		
		
		//使用逐步推到的方法来学习，便于理解
		//第1轮；
				
		/*
		// 定义待插入的数,有序列表的后一个数
		int insertVal = arr[1];
		int insertIndex = 0;//即arr[1]前面数字的下标
		//给insertVal 找到插入的位置
		//说明：
		//1.insertIndex >= 0 保证在给insertVal 插入位置时不越界
		//2.insertVal < arr[insertIndex] 表示待插入的数，还没有找到插入的位置
		//3.就需要将arr[insertIndex]这个值后移
		while (insertIndex >= 0 && insertVal < arr[insertIndex]) {
			arr[insertIndex + 1] = arr[insertIndex];
			//注意：arr[insertIndex + 1]位置的值已经被保存在insertVal中
			insertIndex--;
		}
		//当推迟while循环时，找到插入位置，insertIndex+1;使用debug加深理解
		arr[insertIndex + 1] = insertVal;
		System.out.println("第1轮插入后");
		System.out.println(Arrays.toString(arr));
		
		// 第2轮
		insertVal = arr[2];
		insertIndex = 2 - 1;//即arr[2]前面数字的下标
		
		while (insertIndex >= 0 && insertVal < arr[insertIndex]) {
			arr[insertIndex + 1] = arr[insertIndex];
			//注意：arr[insertIndex + 1]位置的值已经被保存在insertVal中
			insertIndex--;
		}
		
		arr[insertIndex + 1] = insertVal;
		
		System.out.println("第2轮插入后");
		System.out.println(Arrays.toString(arr));
		
		
		// 第3轮
		insertVal = arr[3];
		insertIndex = 3 - 1;// 即arr[3]前面数字的下标

		while (insertIndex >= 0 && insertVal < arr[insertIndex]) {
			arr[insertIndex + 1] = arr[insertIndex];
			// 注意：arr[insertIndex + 1]位置的值已经被保存在insertVal中
			insertIndex--;
		}

		arr[insertIndex + 1] = insertVal;

		System.out.println("第3轮插入后");
		System.out.println(Arrays.toString(arr));
		*/
		
		
	}

}
