package com.yt.search;

// 注意：使用二分查找的前提是数组是有序的。
public class BinarySearch {

	public static void main(String[] args) {
		int arr[] = { 1, 8, 10, 89, 1000, 1234 };

		int resultIndex = binarySearch(arr, 0, arr.length - 1, 12);
		System.out.println("resultIndex=" + resultIndex);

	}

	// 二分查找算法
	/**
	 * 
	 * @param arr
	 *            数组
	 * @param left
	 *            左边索引
	 * @param right
	 *            右边索引
	 * @param findValue
	 *            需要查找的值
	 * @return 如果找到，就返回下标，如果没有找到，就返回-1
	 */
	public static int binarySearch(int[] arr, int left, int right, int findValue) {
		
		// 当left>right时，说明递归整个数组、但是没有找到
		if (left > right) {
			return -1;
		}
		
		int mid = (left + right) / 2;
		int midValue = arr[mid];
		if (findValue > midValue) {
			// 向右递归
			return binarySearch(arr, mid + 1, right, findValue);
		} else if (findValue < midValue) {
			// 向左递归
			return binarySearch(arr, left, mid - 1, findValue);
		} else {
			// 你很幸运，找到了
			return mid;
		}

	}

}
