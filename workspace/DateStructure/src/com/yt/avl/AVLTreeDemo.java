package com.yt.avl;


public class AVLTreeDemo {

	public static void main(String[] args) {
//		int[] arr = {4,3,6,5,7,8};
//		int[] arr = {10,1,8,9,7,6};
		int[] arr = {10,11,7,8,8,9};
		//创建一个AVLTree对象
		AVLTree avlTree = new AVLTree();
		//添加结点
		for(int i = 0; i<arr.length; i++){
			avlTree.add(new Node(arr[i]));
		}
		
		//中序遍历
		System.out.println("中序遍历之后");
		avlTree.infixOrder();
		
		System.out.println("平衡之后=====");
		System.out.println("树的高度=" + avlTree.getRoot().height());//3
		System.out.println("树的左子树高度=" + avlTree.getRoot().leftHeight());//2
		System.out.println("树的右子树高度=" + avlTree.getRoot().rightHeight());//2
		
		
		

	}

}

// 创建AVLTree
class AVLTree {
	private Node root;

	
	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) {
		this.root = root;
	}

	// 编写一个返回要删除结点右子树的最大值
	/**
	 * 
	 * @param node
	 *            传入的结点（当做二叉排序树的根结点）
	 * @return 返回以node为根结点的二叉排序树的最小结点的值
	 */
	public int delRightTreeMin(Node node) {
		Node target = node;
		// 循环的查找左子结点，就会找到最小值
		while (target.left != null) {
			target = target.left;
		}
		// 找到最小值之后，删除最小值
		delNode(target.value);
		// 此时target还是指向最小值结点的
		return target.value;
	}

	// 删除结点
	public void delNode(int value) {
		if (root == null) {
			return;
		} else {
			// 1.先查找要删除的结点
			Node targetNode = search(value);
			// 如果没有找到要删除的结点
			if (targetNode == null) {
				return;
			}
			// 如果发现当前这棵二叉排序树只有一个结点（特例，只有更结点）
			if (root.left == null && root.right == null) {
				root = null;
				return;
			}

			// 2.找到targetNode的父结点
			Node parent = searchParent(value);
			// 如果删除的结点是子结点
			if (targetNode.left == null && targetNode.right == null) {
				// 判断targetNode是父结点的左子结点还是右子结点
				if (parent.left != null && parent.left.value == value) {
					// 是左子结点
					parent.left = null;
				} else if (parent.right != null && parent.right.value == value) {
					// 是右子结点
					parent.right = null;
				}
			} else if (targetNode.right != null && targetNode.left != null) {
				// 删除有两棵子树的结点
				int minVal = delRightTreeMin(targetNode.right);
				targetNode.value = minVal;

			} else {// 删除只有一棵子树的结点

				if (targetNode.left != null) {// 如果删除的结点有左子结点
					if (parent != null) {
						// 如果targrtNode 是 parent 的左子结点
						if (parent.left.value == value) {
							parent.left = targetNode.left;
						} else {// 如果targrtNode 是 parent 的右子结点
							parent.right = targetNode.left;
						}
					} else {
						root = targetNode.left;
					}

				} else {// 如果删除的结点有右子结点
					if (parent != null) {
						// 如果targrtNode 是 parent 的左子结点
						if (parent.left.value == value) {
							parent.left = targetNode.right;
						} else {// 如果targrtNode 是 parent 的右子结点
							parent.right = targetNode.right;
						}
					} else {
						root = targetNode.right;
					}
				}
			}
		}
	}

	// 查找要删除的结点
	public Node search(int value) {
		if (root == null) {
			return null;
		} else {
			return root.search(value);
		}
	}

	// 查找要删除结点的父结点
	public Node searchParent(int value) {
		if (root == null) {
			return null;
		} else {
			return root.searchParent(value);
		}
	}

	// 添加结点方法
	public void add(Node node) {
		if (root == null) {
			root = node;
		} else {
			root.add(node);
		}
	}

	// 中序遍历
	public void infixOrder() {
		if (root != null) {
			root.infixOrder();
		} else {
			System.out.println("二叉排序树为空，不能遍历");
		}
	}
}

// 第一，创建结点信息
class Node {
	int value;
	Node left;
	Node right;

	public Node(int value) {
		super();
		this.value = value;
	}

	// 返回左子树的高度
	public int leftHeight() {
		if (left == null) {
			return 0;
		}
		return left.height();
	}

	// 返回右子树的高度
	public int rightHeight() {
		if (right == null) {
			return 0;
		}
		return right.height();
	}

	// 返回以当前结点为根结点的高度，以该结点为根结点的树的高度
	public int height() {
		return Math.max(left == null ? 0 : left.height(), right == null ? 0 : right.height()) + 1;
	}
	
	// 左旋转方法
	public void leftRotate() {
		//创建新的结点，以当前根结点的值
		Node newNode = new Node(value);
		//把新的结点的左子树设置成当前结点的左子树
		newNode.left = left;
		//把新结点的右子树设置成当前结点的右子结点的左子树
		newNode.right = right.left;
		//把当前结点的值替换成当前结点的右子结点的值
		value = right.value;
		//把当前结点的右子树设置成当前结点的右子树的右子树
		right = right.right;//相当于跳过一个结点
		//把当前结点的左子树（左子结点）设置成新的结点
		left = newNode;		
	}
	
	//右旋转方法
	public void rightRotate(){
		//1.创建一个新结点newNode，新结点的值等于当前结点的值
		Node newNode = new Node(value);
		//2.把新结点的右子树设置成当前结点的右子树
		newNode.right = right;
		//3.把新结点的左子树设置成当前结点的左子树的右子树
		newNode.left = left.right;
		//4.把当前结点的值换成当前结点的左子结点
		value = left.value;
		//5.把当前结点的左子树设置成左子树的左子树
		left = left.left;
		//6.把当前结点的右子树设置成新结点（难）
		right = newNode;
	}

	@Override
	public String toString() {
		return "Node [value=" + value + "]";
	}

	// 编写查找需要删除结点的父结点
	/**
	 * 
	 * @param value
	 *            要查找的结点值
	 * @return 返回的是要删除结点的父结点，如果没有就返回null
	 */
	public Node searchParent(int value) {
		// 如果当前结点就是要删除结点的父结点，返回该结点
		if ((this.left != null && this.left.value == value) || (this.right != null && this.right.value == value)) {
			return this;
		} else {
			// 如果查找的值小于当前结点的值，并且当前结点的左子结点不为空
			if (value < this.value && this.left != null) {
				return this.left.searchParent(value);// 向左子树递归查找
			} else if (value >= this.value && this.right != null) {
				return this.right.searchParent(value);
			} else {
				return null;// 没有找到父结点
			}
		}

	}

	// 编写查找要删除的结点
	/**
	 * 
	 * @param value
	 *            需要删除的结点
	 * @return 如果找到则返回该结点，否则返回null
	 */
	public Node search(int value) {
		if (this.value == value) {
			// 找到删除的结点，返回
			return this;
		} else if (value < this.value) {
			// 如果查找的结点值小于当前结点，向左子树递归查找
			// 注意，判断左子结点是否为空
			if (this.left == null) {
				return null;// 没有找到
			}
			return this.left.search(value);
		} else {
			// 如果查找的结点值大于当前结点，向右子树递归查找
			// 注意，判断右子结点是否为空
			if (this.right == null) {
				return null;// 没有找到
			}
			return this.right.search(value);
		}

	}

	// 编写中序遍历方法（前序，后序遍历复习前面的）
	public void infixOrder() {
		if (this.left != null) {
			this.left.infixOrder();
		}
		System.out.println(this);
		if (this.right != null) {
			this.right.infixOrder();
		}

	}

	// 编写增加结点的方法
	public void add(Node node) {
		if (node == null) {
			return;
		}
		// 判断当前结点与左右子树值的大小关系
		if (node.value < this.value) {
			// 左子树的值小，向左子树增加
			if (this.left == null) {
				// 左子树为空，直接添加
				this.left = node;
			} else {
				// 左子树不为空,向左子树增加
				this.left.add(node);
			}
		} else {
			// 传入的node结点的值大于当前结点，增加到右子树
			if (this.right == null) {
				this.right = node;
			} else {
				// 递归增加到右子树
				this.right.add(node);
			}
		}
		
		//当添加完一个结点之后，如果 （右子树的高度-左子树的高度）> 1,则左旋转
		if (rightHeight() - leftHeight() > 1) {
			// 如果它的右子树的左子树的高度大于它的右子树的高度
			if (right != null && right.leftHeight() > right.rightHeight()) {
				// 先对当前结点的右节点（右子树）进行右旋转
				right.rightRotate();
				// 再对当前结点进行左旋转
				leftRotate();
			} else {
				// 直接左旋转
				leftRotate();
			}
			return;//必须要
		}
		
		// 当添加完一个结点之后，如果 （左子树的高度-右子树的高度）> 1,则右旋转
		if (leftHeight() - rightHeight()  > 1) {
			// 如果它的左子树的右子树的高度大于它的左子树的高度
			if (left != null && left.rightHeight() > left.leftHeight()) {
				//先对当前结点的左节点（左子树）进行左旋转
				left.leftRotate();
				//再对当前结点进行右旋转
				rightRotate();
			} else {
				//直接右旋转
				rightRotate();
			}
			
		}

	}

}
