package com.yt.dynamic;

public class KnapstackProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] w = {1,4,3};//物品的重量
		int[] val = {1500,3000,2000};//物品的价值，这里val[i]表示前面笔记中的v[i]
		int m = 4;//背包的容量,最多可以装多少东西
		int n = val.length;//表示物品的个数，与价格的数量是相等的
		
		// 创建二维数组
		//v[i][j] 表示在第i个物品中能装入容量为j的背包的最大价值
		int[][] v = new int[n+1][m+1];
		//为了记录商品的放入情况，定义一个二维数组
		int[][] path = new int[n+1][m+1];
		
		//初始化第一行和第一列，当然可以不用处理，以为默认值都是0
		for(int i = 0; i < v.length; i++){
			v[i][0] = 0;//将第一列设置为0
		}
		for(int i = 0; i < v[0].length; i++){
			v[0][i] = 0;//将第一列设置为0
		}
		
		//根据前面的公式来动态规划处理
		for(int i = 1; i < v.length; i++){//不处理第一行，i是从1开始的
			for(int j = 1; j < v[0].length; j++){//不处理第一列，j是从1开始的
				//带入公式处理动态规划问题
				if (w[i-1] > j) {
					//因为程序i是从1开始的，因此原来公式中w[i] 要修改成w[i-1]
					v[i][j] = v[i-1][j];
				} else {
					//说明：注意下标需要减1的位置
					v[i][j] = Math.max(v[i-1][j], val[i-1] + v[i-1][j-w[i-1]]);
					//为了记录商品存放到背包的情况，不能直接使用上述公式，使用if-else来代替
					if (v[i-1][j] < val[i-1] + v[i-1][j-w[i-1]]) {
						v[i][j] = val[i-1] + v[i-1][j-w[i-1]];
						// 把当前的情况记录到path中
						path[i][j] = 1;
					} else {
						v[i][j] = v[i-1][j];
					}
					
				}
			}
		}
		
		//输出v
		for(int i = 0; i < v.length; i++){
			for(int j = 0; j < v[i].length; j++){
				System.out.print(v[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.println("===============================");
		// 输出最后放入的商品是哪些
		//遍历path,输出所有的放入商品的情况,	其实我们只需要最后一次的输出
//		for(int i =0; i<path.length;i++){
//			for(int j=0; j<path[i].length;j++){
//				if (path[i][j] == 1) {
//					System.out.printf("第%d个商品放入到背包\n" , i);
//				}				
//			}
//		}
//		
		//动脑筋
		int i = path.length -1;//行的最大下标
		int j = path[0].length -1;//列的最大下标
		while(i>0 && j>0){
			//从path的最后开始
			if (path[i][j] == 1) {
				System.out.printf("第%d个商品放入到背包\n" , i);
				j -= w[i-1];
			}
			i--;
		}
		

	}

}
