package com.yt.huffmantree;

public class TestComparable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student student1 = new Student();
		student1.setAge(17);
		student1.setName("zhangsan");
		
		Student student2 = new Student();
		student2.setAge(19);
		student2.setName("lisi");
		
		Comparable max = getMax(student1, student2);
		System.out.println(max);

	}

	public static Comparable getMax(Comparable s1, Comparable s2) {
		int cmp = s1.compareTo(s2);
		if (cmp >= 0) {
			return s1;
		} else {
			return s2;
		}
	}

}

class Student implements Comparable<Student>{
	private int age;
	private String name;
	
	public Student() {
		
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	@Override
	public String toString() {
		return "Student [age=" + age + ", name=" + name + "]";
	}

	// 必须编写重载方法
	@Override
	public int compareTo(Student o) {
		// TODO Auto-generated method stub
		// 定义比较规则
		return this.getAge() - o.getAge();
	}
	
	
	
}