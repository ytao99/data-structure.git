package com.yt.tree;

import java.util.Arrays;

public class HeapSort {

	public static void main(String[] args) {
		//要求将数组进行升序排序
		int arr[] = {4,6,8,5,9};
		heapSort(arr);

	}
	
	// 编写堆排序方向
	public static void heapSort(int[] arr){
		
		int temp = 0;
		System.out.println("堆排序");
		
//		// 分步完成
//		adjustHeap(arr, 1, arr.length);
//		System.out.println("第一次排序" + Arrays.toString(arr));
//		
//		adjustHeap(arr, 0, arr.length);
//		System.out.println("第2次排序" + Arrays.toString(arr));
//		
		// 将待排序序列构造成一个大顶堆
		for(int i = arr.length/2-1; i>=0;i--){
			adjustHeap(arr, i, arr.length);
		}
		
		for(int j = arr.length-1;j>0;j--){
			//交换
			temp = arr[j];
			arr[j] = arr[0];//arr[0]是大顶堆的最大值
			arr[0] = temp;
			// 交换完之后就不是大顶堆了，需要重新再次调整为大顶堆
			adjustHeap(arr, 0, j);
		}
		
		System.out.println("数组=" + Arrays.toString(arr));
	}
	
	//将一个数组（二叉树）,调整成一个大顶堆
	/**
	 * 功能：完成将i对应的非叶子结点的树调整成大顶堆
	 * @param arr 需要调整为大顶堆的数组
	 * @param i 表示非叶子结点的索引
	 * @param length 表示需要对多少个数据进行调整，length是在逐渐减小的
	 */
	public static void adjustHeap(int arr[], int i, int length){
		int temp = arr[i];// 先取出当前元素的值，保存在临时变量
		
		// 说明：k = i*2 + 1 表示i结点的左子结点
		for(int k = i*2 +1; k<length;k = k*2 +1){
			// arr[k]<arr[k+1] 说明左子结点的值小于右子结点的值；
			// k+1 < length 可以不加，但是会降低执行的效率
			if (arr[k]<arr[k+1] && k+1 < length) {
				k++;// k指向右子结点，为了找到局部的最大值
			}
			// 比较左右两个结点的大小之后，找到较大的一个数值
			if (arr[k] > temp) {//再与局部父节点比较大小
				arr[i] = arr[k];// 把较大的值赋给当前结点
				i = k;//重点：i指向k,继续循环比较，因为可能还存在左右子树
			} else {
				break;//为什么可以退出循环？因为比较是从左到右，从下到上调整的
			}
		}
		// for循环结束之后，以i为父结点的数的最大值已经放在了最顶（局部）
		arr[i] = temp;
		
	}

}
