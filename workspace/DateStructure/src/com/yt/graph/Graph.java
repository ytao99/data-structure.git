package com.yt.graph;

import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Graph {

	private ArrayList<String> vertexList;//存储顶点集合
	private int[][] edges;//存储图对应的领接矩阵
	private int numOfEdges;//表示边的数目
	private boolean[] isVisited;//表示是否被访问过
	
	public static void main(String[] args) {
		// 测试图的创建
		int n = 5;//结点的个数
		String vertexs[] = {"A", "B", "C", "D", "E"};
		//创建图
		Graph graph = new Graph(n);
		//添加结点
		for(String vertex : vertexs){
			graph.insertVertex(vertex);
		}
//		graph.showGraph();//没有添加边，输出的全部是0
		// 添加边 案例中的图 A-B A-C B-C B-D B-E
		graph.insertEdges(0, 1, 1);
		graph.insertEdges(0, 2, 1);
		graph.insertEdges(1, 2, 1);
		graph.insertEdges(1, 3, 1);
		graph.insertEdges(1, 4, 1);
		//显示邻接矩阵
		graph.showGraph();
		
		//测试dfs遍历
		System.out.println("深度遍历");
		graph.dfs();
		
		System.out.println();
		System.out.println("广度优先遍历");
		graph.bfs();

	}
	
	// 构造器，进行初始化，
	// n表示结点的个数
	public Graph(int n){
		//初始化矩阵和vertexList
		edges = new int[n][n];
		vertexList = new ArrayList<String>(n);
		numOfEdges = 0;	
		isVisited = new boolean[5];
	}
	//得到第一个邻接结点的下标w
	/**
	 * 
	 * @param index
	 * @return 如果存在就返回对应的下标，否则返回-1
	 */
	public int getFirstNeighbor(int index){
		for(int j = 0; j < vertexList.size(); j++){
			if (edges[index][j] > 0) {
				return j;
			}
		}
		return -1;
	}
	// 根据前一个邻接结点的下标来获取下一个邻接结点
	public int getNextNeighbor(int v1, int v2) {
		for(int j = v2+1; j < vertexList.size(); j++){
			if (edges[v1][j]>0) {
				return j;
			}
		}
		return -1;
	}
	
	//深度优先遍历算法
	//i 第一次是0
	public void dfs(boolean[] isVisited, int i){
		// 首先我们访问该结点，输出
		System.out.print(getValueByIndex(i) + "->");
		//将结点设置为已经访问
		isVisited[i] = true;
		//查找结点i的第一个邻接结点w
		int w = getFirstNeighbor(i);
		while(w != -1){//说明有邻接矩阵没有被访问过
			if (!isVisited[w]) {//如果w这个结点没有被访问，就继续深度优先遍历
				dfs(isVisited, w);
			}
			//如果w结点已经被访问过
			w = getNextNeighbor(i, w);//不是很能理解
		}		
	}
	//对dfs()进行重载，遍历所有的结点，并进行dfs
	public void dfs(){
		isVisited = new boolean[vertexList.size()];
		//遍历所有的结点，进行dfs(回溯)
		for(int i=0; i<getNumOfVertex(); i++){
			if (!isVisited[i]) {
				dfs(isVisited,i);
			}
		}
	}
	
	// 对一个结点进行广度优先遍历的方法（注意这个方法仅仅是对一个结点，对所有的结点需要重载该方法）
	public void bfs(boolean[] isVisited, int i) {
		int u;// 表示队列的头结点对应的下标
		int w;// 邻接结点w
		// 队列，记录结点访问的顺序
		LinkedList<Object> queue = new LinkedList<>();
		// 访问结点，也就是输出结点的信息
		System.out.print(getValueByIndex(i) + "->");
		// 标记已经访问过的结点
		isVisited[i] = true;
		// 将已经访问过的结点加入队列
		queue.addLast(i);

		while (!queue.isEmpty()) {
			// 取出队列的头结点的下标
			u = (Integer) queue.removeFirst();
			// 得到第一个邻接结点的下标w
			w = getFirstNeighbor(u);
			while (w != -1) {// 找到
				// 判断是否访问过找到的这个结点
				if (!isVisited[w]) {
					// 没有访问过，输出该结点的信息
					System.out.print(getValueByIndex(w) + "->");
					// 标记已经访问过
					isVisited[w] = true;
					// 入队
					queue.addLast(w);
				}
				// 以u为前驱结点，找到w后面的下一个邻接点
				w = getNextNeighbor(u, w);// 这句代码就体现了广度优先遍历
			}
		}
	}
	// 遍历所有的结点，都进行广度优先搜索
	public void bfs(){
		isVisited = new boolean[vertexList.size()];//默认值为0，表示没有访问过
		for(int i=0; i<getNumOfVertex(); i++){
			if (!isVisited[i]) {
				bfs(isVisited,i);
			}
		}
	}
	
	
	// 图中常用的方法
	// 返回结点的个数
	public int getNumOfVertex(){
		return vertexList.size();
	}
	//得到边的数目
	public int getNumOfEdges(){
		return numOfEdges;
	}
	//返回结点i(下标)对应的数据0->"A" 1->"B" 2->"C"
	public String getValueByIndex(int i){
		return vertexList.get(i);
	}
	//返回v1和v2之间的权值
	public int getWeight(int v1, int v2){
		return edges[v1][v2];
	}
	//显示图对应的矩阵
	public void showGraph(){
		for(int[] link : edges){
			System.out.println(Arrays.toString(link));//一排一排的输出来
		}
	}
	
	//插入结点
	public void insertVertex(String vertex){
		vertexList.add(vertex);
	}
	// 添加边
	/**
	 * 
	 * @param v1 表示点的下标，是第几个顶点
	 * @param v2 第二个顶点对应的下标
	 * @param weight 两个顶点之间的权重
	 */
	public void insertEdges(int v1, int v2, int weight){
		edges[v1][v2] = weight;
		edges[v2][v1] = weight;
		numOfEdges++;
	}
	

}
