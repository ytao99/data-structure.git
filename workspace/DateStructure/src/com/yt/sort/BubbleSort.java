package com.yt.sort;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class BubbleSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//int arr[] = {3,9,-1,10,-20};
		
		//测试冒泡排序的速度
		//创建一个80000个随机的数组
		int[] arr = new int[80000];
		for(int i = 0; i < 80000;i++) {
			arr[i] = (int)(Math.random()*800000);
		}
		
		//System.out.println(Arrays.toString(arr));
		Date date1 = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date1Str = simpleDateFormat.format(date1);
		System.out.println("排序前的时间是=" + date1Str);
		
		//测试冒泡排序
		bubbleSort(arr);
		//System.out.println(Arrays.toString(arr));
		
		Date date2 = new Date();
		//SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date2Str = simpleDateFormat.format(date2);
		System.out.println("排序后的时间是=" + date2Str);
		//大概是8秒的时间
		
		
		
		//为了更深刻的理解，将每一次排序的结果打印出来
		
		/*
		//第一趟排序，将最大的一个数排在最后
		//定义一个临时变量
		int temp = 0;
		for (int j = 0; j < arr.length - 1; j++) {
			if (arr[j] > arr[j+1]) {//前面的数比后面的数大就交换
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
		
		System.out.println("第一趟排序结束之后：");
		System.out.println(Arrays.toString(arr));
		
		//第2趟排序
		for (int j = 0; j < arr.length - 1 - 1; j++) {
			if (arr[j] > arr[j+1]) {
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
		
		System.out.println("第2趟排序结束之后：");
		System.out.println(Arrays.toString(arr));
		
		// 第3趟排序
		for (int j = 0; j < arr.length - 1 - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}

		System.out.println("第3趟排序结束之后：");
		System.out.println(Arrays.toString(arr));
		
		// 第4趟排序
		for (int j = 0; j < arr.length - 1 - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}

		System.out.println("第4趟排序结束之后：");
		System.out.println(Arrays.toString(arr));
		*/
		
		/*
		// 第一趟排序
		int temp = 0;
		for(int i = 0; i < arr.length -1; i++){//循环的次数；（大循环）
			for (int j = 0; j < arr.length - 1 - i; j++) {//每一次小循环需要比较的次数
				if (arr[j] > arr[j + 1]) {
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
			System.out.println("第"+ (i+1) + "趟排序结束之后：");
			System.out.println(Arrays.toString(arr));

		}
		*/
		

	}
	//将冒泡排序算法封装成一个方法，优化方法：有一次没有进行比较就结束排序了
	public static void bubbleSort(int[] arr) {
		int temp = 0;//临时变量
		boolean flag = false;//标识变量，表示是否进行过交换（代码小技巧）
		for(int i = 0; i < arr.length - 1; i++) {
			// n个数需要n-1次比较
			for(int j = 0; j < arr.length - 1; j++) {
				if (arr[j] >arr[j+1]) {
					flag = true;//发生交换，重新修改flag的值
					temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
			//比较完毕之后
			if (!flag) {
				//flag的值是false,也就是说：flag = true;没有执行
				break;
			}else {
				flag = false;//有比较过程，再一次修改flag的值
			}
		}
	}
}
