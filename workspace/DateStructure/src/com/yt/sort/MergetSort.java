package com.yt.sort;

import java.util.Arrays;

public class MergetSort {

	public static void main(String[] args) {
		int arr[] = {8,4,5,7,1,3,6,2};
		int temp[] = new int[arr.length];// 说明归并排序需要额外的空间
		mergeSort(arr, 0, arr.length-1, temp);
		System.out.println("归并排序后=" + Arrays.toString(arr));

	}
	
	// 分+合的方法
	public static void mergeSort(int[] arr, int left, int right, int[] temp){
		if (left < right) {
			int mid = (left + right) / 2;// 中间索引
			// 向左递归进行分解
			mergeSort(arr, left, mid, temp);
			// 向右递归分解
			// 注意： mid + 1 ,mid值在向左时已经处理
			mergeSort(arr, mid + 1, right, temp);
			// 难点：合并
			merge(arr, left, mid, right, temp);
		}
	}
	
	
	
	// 合并的方法（有难度）
	/**
	 * 
	 * @param arr 排序的原始数组
	 * @param left 左边有序序列的初始索引
	 * @param mid 中间索引
	 * @param right 右边索引
	 * @param temp 中转数组
	 */
	public static void merge(int[] arr, int left, int mid, int right, int[] temp){
		
//		System.out.println("输出次数");
		
		int i = left;// 初始化i,左边有序序列的初始索引
		int j = mid + 1;// 初始化j,右边有序序列的初始索引
		int t = 0;// 指向临时temp数组的当前索引
		
		// 第一步
		// 先把左右两边（有序）的数据按照规则填充到temp数组
		// 直到左右两边的有序序列，有一边处理完毕为止
		while (i <= mid && j <= right) {// 条件满足，继续进行循环
			// 如果左边的当前元素小于或等于右边有序序列的当前元素
			// 即将左边的当前元素拷贝到temp数组
			// 然后后移下标
			if (arr[i] <= arr[j]) {
				temp[t] = arr[i];
				i++;
				t++;
			} else {
				temp[t] = arr[j];
				j++;
				t++;
			}

		}
		
		// 第二步
		// 把有剩余数据的一边的数据依次全部填充到temp中
		while(i <= mid){// 说明左边的有序序列有剩余,就全部填充到temp数组中
			temp[t] = arr[i];
			i++;
			t++;
		}
		while(j <= right){// 说明右边的有序序列有剩余,就全部填充到temp数组中
			temp[t] = arr[j];
			j++;
			t++;
		}
		
		// 第三步
		// 将temp数组的元素拷贝到arr中
		// 注意：并不是每次都拷贝所有
		t = 0;
		int tempLeft = left;
		// 难点1
		System.out.println("tempLeft=" + tempLeft + ";right" + right);
		while (tempLeft <= right) {
			arr[tempLeft] = temp[t];
			tempLeft++;
			t++;
		}
		
	}
	

}
