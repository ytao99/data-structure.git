package com.yt.hashtab;

import java.util.Scanner;

public class HashTabDemo {

	public static void main(String[] args) {
		
		// 创建哈希表
		HashTab hashTab = new HashTab(7);
		
		// 写一个简答的菜单
		String key = "";
		Scanner scanner = new Scanner(System.in);
		while(true){
			System.out.println("add:添加雇员");
			System.out.println("find:查找雇员");
			System.out.println("list:显示雇员");
			System.out.println("exit：退出系统");
			
			key = scanner.next();
			switch (key) {
			case "add":
				System.out.println("输入id");
				int id = scanner.nextInt();	
				System.out.println("输入名字");
				String name = scanner.next();
				// 创建雇员
				Emp emp = new Emp(id, name);
				hashTab.add(emp);
				break;
			case "list":
				hashTab.list();
				break;
			case "find":
				System.out.println("请输入要查找的id");
				id = scanner.nextInt();
				hashTab.findEmpById(id);
				break;
			case "exit":
				scanner.close();
				System.exit(0);

			default:
				break;
			}
		}

	}
	

}

// 创建哈希表HashTab,管理多条链表
class HashTab{
	private EmpLinkedList[] empLinkedLists;
	private int size;// 表示有多少条链表
	
	// 构造器
	public HashTab(int size){
		this.size = size;
		// 初始化empLinkedLists
		empLinkedLists = new EmpLinkedList[size];
		// 留一个问题？
		// 不要忘了分别初始化每一个链表
		for(int i = 0; i < size; i++){
			empLinkedLists[i] = new EmpLinkedList();
		}
		
		
	}
	
	// 添加雇员
	public void add(Emp emp){
		// 根据员工的id,得到该员工应该添加到哪条链表
		int empLinkedListNo = hashFun(emp.id);
		// 将emp添加到对应的链表中
		empLinkedLists[empLinkedListNo].add(emp);
		
	}
	
	// 遍历所有的链表，遍历hashtab
	public void list(){
		for(int i = 0; i < size; i++){
			empLinkedLists[i].list(i);
		}
	}
	
	// 根据输入的id查找雇员
	public void findEmpById(int id){
		//使用散列函数确定到哪条链表查找
		int empLinkedListNo = hashFun(id);
		Emp emp = empLinkedLists[empLinkedListNo].findEmpById(id);
		if (emp != null) {
			// 找到
			System.out.printf("在第%d条链表中找到雇员 id= %d\t",(empLinkedListNo + 1),id);
		} else {
			System.out.println("没有找到");
		}
		
	}
	
	
	// 编写散列函数，使用一个简单取模法
	public  int hashFun(int id){
		return id % size;
	}
	
}


// 表示一个雇员
class Emp{
	public int id;
	public String name;
	public Emp next;//next默认为空
	public Emp(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
}

// 创建EmpLinkedList 表示链表
class EmpLinkedList{
	// 头指针，执行第一个Emp，因此这个链表的head是直接指向第一个Emp
	private Emp head;//默认null
	
	// 添加员工到链表
	// 说明
	// 1.假定添加雇员是，id是自增长的，即id是从小到大
	// 因此直接加到最后即可
	public void add(Emp emp){
		// 如果是添加第一个雇员
		if(head == null){
			head = emp;
			return;
		}
		
		// 如果不是第一个雇员，利用辅助指针
		Emp curEmp = head;
		while(true){
			if (curEmp == null) {
				// 说明到链表的最后
				break;
			}
			curEmp = curEmp.next;// 后移，
		}
		
		// 退出时，直接将emp加入链表
		curEmp.next = emp;
	}
	
	// 遍历链表的雇员信息
	public void list(int no){
		if (head == null) {
			System.out.println("第"+ (no + 1) +"链表为空");
			return;
		}
		System.out.println("第"+ (no + 1) +"链表的信息为：");
		Emp curEmp = head;
		while(true){
			System.out.printf("=> id=%d name=%s\t",curEmp.id,curEmp.name);
			if (curEmp.next == null) {
				// 说明已经到达最后节点
				break;
			}
			curEmp = curEmp.next;//后移，遍历
		}
		System.out.println();
				
	}
	// 根据id查找雇员
	// 如果查找到就返回Emp,否则返回null
	public Emp findEmpById(int id){
		// 判断链表是否为空
		if(head == null){
			System.out.println("链表为空");
			return null;
		}
		//辅助指针
		Emp curEmp = head;
		while(true){
			if (curEmp.id == id) {
				// 找到
				break;//这时curEmp就指向要查找的雇员
			}
			// 退出
			if (curEmp.next == null) {
				// 没有找到
				curEmp= null;
				// 记得退出循环
				break;
			}
			
			curEmp = curEmp.next;// 后移
			
		}
		return curEmp;
		
	}
	
}




