package com.yt.greedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class GreedyAlgorithm {

	public static void main(String[] args) {
		// 创建广播电台，放入到Map中
		HashMap<String, HashSet<String>> broadscasts = new HashMap<String,HashSet<String>>();
		//将各个电台放入到broadcasts中
		HashSet<String> hashSet1 = new HashSet<String>();
		hashSet1.add("北京");
		hashSet1.add("上海");
		hashSet1.add("天津");
		
		HashSet<String> hashSet2 = new HashSet<String>();
		hashSet2.add("广州");
		hashSet2.add("北京");
		hashSet2.add("深圳");
		
		HashSet<String> hashSet3 = new HashSet<String>();
		hashSet3.add("成都");
		hashSet3.add("上海");
		hashSet3.add("杭州");
		
		HashSet<String> hashSet4 = new HashSet<String>();
		hashSet4.add("上海");
		hashSet4.add("天津");
		
		HashSet<String> hashSet5 = new HashSet<String>();
		hashSet5.add("杭州");
		hashSet5.add("大连");
		
		//加入到map
		broadscasts.put("K1", hashSet1);
		broadscasts.put("K2", hashSet2);
		broadscasts.put("K3", hashSet3);
		broadscasts.put("K4", hashSet4);
		broadscasts.put("K5", hashSet5);
		
		//存放所有的地区
		HashSet<String> allAreas = new HashSet<String>();
		allAreas.add("北京");
		allAreas.add("上海");
		allAreas.add("天津");
		allAreas.add("广州");
		allAreas.add("深圳");
		allAreas.add("成都");
		allAreas.add("杭州");
		allAreas.add("大连");
		
		//创建ArrayList存放选择的电台集合
		ArrayList<String> selects = new ArrayList<String>();
		
		//定义一个临时集合，保存在遍历过程中的电台覆盖的地区和当前还没有覆盖的地区的交集
		HashSet<String> tempSet = new HashSet<String>();
		
		//定义一个maxKey,保存在一次遍历过程中，能够覆盖最大未覆盖的地区对应的电台的key
		//如果maxKey不为null，则会加入到selects中
		String maxKey = null;
		while(allAreas.size() != 0){//如果allAreas不为0，则表示还没有覆盖所有区域
			// 每进行一次while需要置空
			maxKey = null;
			
			//遍历broadcasts,取出对应的key
			for(String key: broadscasts.keySet()){
				//每一次循环都需要clear(()
				tempSet.clear();
				
				//当前这个key能够覆盖的区域
				HashSet<String> areas = broadscasts.get(key);
				tempSet.addAll(areas);
				//求出tenpSet 和 allAreas 集合的交集，交集会赋值给tempSet
				tempSet.retainAll(allAreas);
				//如果当前这个集合包含的未覆盖地区的数量，比maxKey指向的集合地区还多，需要重置maxKey
				//tempSet.size()>broadscasts.get(maxKey).size()) 贪心算法核心
				if (tempSet.size()>0 &&
						(maxKey==null || tempSet.size()>broadscasts.get(maxKey).size())) {
					maxKey = key;
				}				
			}
			if (maxKey!= null) {
				selects.add(maxKey);
				//将maxKey指向的广播电台覆盖的地区，从allAreas去掉
				allAreas.removeAll(broadscasts.get(maxKey));
			}
			
		}
		
		System.out.println("得到的选择结果是："+ selects);

	}

}
