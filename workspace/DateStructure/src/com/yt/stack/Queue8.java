package com.yt.stack;

public class Queue8 {

	//定义一个max表示一共有多少个黄后
	int max = 8;
	//定义一个数组表示存放皇后位置的结果
	//下标表示行，数组的值表示列
	int array[] = new int[max];
	static int count = 0;
	public static void main(String[] args) {
		Queue8 queue8 = new Queue8();
		queue8.check(0);
		System.out.printf("一共有%d解法",count);

	}
	
	//编写一个方法，放置第n个皇后
	private void check(int n) {
		if(n==max){
			//n=8表示8个皇后都已经放好
			print();
			return;
		}
		//依次放入皇后，判断是否冲突
		for(int i = 0; i < max; i++){
			//先放到当前位置的第一列
			array[n] = i;
			//判断是否冲突
			if (jugle(n)) {
				//不冲突，开始下一个位置，即递归
				check(n+1);
			}
			//冲突，继续执行上面的array[n] = i;
		}
		
	}
	
	//查看当我们放置第n个皇后之后是否和前面已经摆放的皇后冲突，
	//注意：n是从0开始的
	private boolean jugle(int n) {
		//说明：
		//1.array[i]==array[n]表示判断第n个皇后是否和前面的n-1个在同一列
		//2.Math.abs(n-i)==Math.abs(array[n]-array[i]判断是否在同一斜线
		//3.n本身表示的意思就是列，不用判断列是否冲突
		for(int i = 0; i < n; i++){
			if(array[i]==array[n] || Math.abs(n-i)==Math.abs(array[n]-array[i])){
				return false;
			}
		}
		return true;
	}
	
	//写一个方法可以输出摆放的位置
	private void print() {
		count++;
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

}
